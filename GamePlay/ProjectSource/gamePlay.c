
/****************************************************************************
 Module
   TestHarnessService0.c

 Revision
   1.0.1

 Description
   This is the first service for the Test Harness under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/26/17 18:26 jec     moves definition of ALL_BITS to ES_Port.h
 10/19/17 21:28 jec     meaningless change to test updating
 10/19/17 18:42 jec     removed referennces to driverlib and programmed the
                        ports directly
 08/21/17 21:44 jec     modified LED blink routine to only modify bit 3 so that
                        I can test the new new framework debugging lines on PF1-2
 08/16/17 14:13 jec      corrected ONE_SEC constant to match Tiva tick rate
 11/02/13 17:21 jec      added exercise of the event deferral/recall module
 08/05/13 20:33 jec      converted to test harness service
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// This module
#include "../ProjectHeaders/gamePlay.h"
#include "../ProjectHeaders/SimplePWM.h"
#include "../ProjectHeaders/SimpleTimerModule.h"

#include "../ProjectHeaders/spiHAL.h"

// debugging printf()
//#include "dbprintf.h"

// Hardware
#include <xc.h>
#include <proc/p32mx170f256b.h>
#include <sys/attribs.h> // for ISR macors

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Port.h"

/*----------------------------- Module Defines ----------------------------*/
// these times assume a 10.000mS/tick timing
#define ONE_SEC 1000
#define HALF_SEC (ONE_SEC / 2)
#define TWO_SEC (ONE_SEC * 2)
#define FIVE_SEC (ONE_SEC * 5)

#define ENTER_POST     ((MyPriority<<3)|0)
#define ENTER_RUN      ((MyPriority<<3)|1)
#define ENTER_TIMEOUT  ((MyPriority<<3)|2)

//#define TEST_HARDWARE


//#define ARRAY_SIZE(x) (sizeof(x)/sizeof(x[0]))
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/



void IOsetupt();


/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
// add a deferral queue for up to 3 pending deferrals +1 to allow for overhead
static ES_Event_t DeferralQueue[2 + 1];

volatile static uint16_t hall_sensor_1;
volatile static uint16_t hall_sensor_2;

static uint8_t SPI2_BUFFER[50];
static uint8_t SPI2_BUFFER_SIZE;


GameState_t currentState = InitGameElements;


/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTestHarnessService0

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitGamePlay(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  // initialize deferral queue for testing Deferal function
  //ES_InitDeferralQueueWith(DeferralQueue, ARRAY_SIZE(DeferralQueue));
  // initialize LED drive for testing/debug output


  // Hall sensor starts off high.
  hall_sensor_1 = 1;
  hall_sensor_2 = 1;
  
  for (int i = 0; i<100000; i++){
      
  }
  
  // initialize the Short timer system for channel A
  //ES_ShortTimerInit(MyPriority, SHORT_TIMER_UNUSED);

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTestHarnessService0

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostGamePlay(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTestHarnessService0

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunGamePlay(ES_Event_t ThisEvent)
{
    ES_Event_t ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
    static char DeferredChar = '1';
  
    GameState_t nextState = currentState;
    
    //printf("Hi %s\n\r",currentState);
  
    switch (currentState) 
    {
        case InitGameElements:
        {
            switch (ThisEvent.EventType)
            {
                case ES_INIT:
                {
                    Timer2Init();
                    //Timer5Init();
                    PWM1Init();
                    PWM1DutyCycle(3);
                    IOsetupt();
                    SPI2Setup();
                    SPI1Setup();
                    
                    // Team Selection
                    if (PORTAbits.RA2 == 1) {
                        LATAbits.LATA3 = 1;
                        LATAbits.LATA4 = 0;
                    } else {
                        LATAbits.LATA3 = 0;
                        LATAbits.LATA4 = 1;
                    }
                 
                    
                    //printf("Position Switch A = %d\n\r", PORTBbits.RB9);
                    //printf("Position Switch B = %d\n\r", PORTBbits.RB12);
                    
                    if (PORTBbits.RB9 == 1 && PORTBbits.RB12 == 0) {
                        nextState = Relay;
                        printf("Start Position: Relay\n\r");
                        uint8_t mod2[] = {'r', 0x00};
                        SPI2Write(mod2,ARRAY_SIZE(mod2));
                        PWM1DutyCycle(12);
                        
                    } else if (PORTBbits.RB9 == 0 && PORTBbits.RB12 == 1) {
                        nextState = Waiting;
                        printf("Start Position: Waiting\n\r");
                        uint8_t mod2[] = {'w', 0x00};
                        SPI2Write(mod2,ARRAY_SIZE(mod2));
                        PWM1DutyCycle(3);
                        
                    } else if (PORTBbits.RB9 == 1 && PORTBbits.RB12 == 1) {
                        nextState = Transmission;
                        printf("Start Position: Transmission\n\r");
                        uint8_t mod2[] = {'s', 0x00};
                        SPI2Write(mod2,ARRAY_SIZE(mod2));
                        PWM1DutyCycle(3);
                        
                    } else {
                        printf("ERROR in position selection\n\r ");
                        LATAbits.LATA3 = 1;
                        LATAbits.LATA4 = 1;
                    }
                    //nextState = Waiting;
                    #ifdef TEST_HARDWARE
                        nextState = HardwareTesting;
                    #endif

                    //ES_Event_t SPI_Event;
                    //SPI_Event.EventType = ES_SPI2_BEGIN;
                    //PostGamePlay(SPI_Event);
                   // ES_Timer_InitTimer(SERVICE0_TIMER, FIVE_SEC);

                    puts("Service 00:");
                    printf("\rES_INIT received in Service %d\r\n", MyPriority);
                }
                break;
            }
        }
        break;

        case Waiting:
        {
            switch (ThisEvent.EventType){
               
                case ES_IR_REQUEST_TX:
                {
                    printf("Transmission received, entering relay state\n\r");
                    uint8_t mod2[] = {'r', 0x00};
                    SPI1Write(mod2,ARRAY_SIZE(mod2));
                    PWM1DutyCycle(12);
                    nextState = Relay;
                }
                break;
            }
                
        }
        break;

        case Relay:
        {
            switch (ThisEvent.EventType){
                printf("Relay node\n\r");
                case ES_HALL1_SENSOR_MAGNET_ENTERS:
                {
                    printf("Next robot detected, stopping\n\r");
                    uint8_t mod[] = {'s', 0x00};
                    SPI1Write(mod,ARRAY_SIZE(mod));
                    SPI2Write(mod,ARRAY_SIZE(mod));
                    nextState = Transmission;
                }
                break;
            }
        }
        break;
        
        case Transmission:
        {
            switch (ThisEvent.EventType){
                case ES_HALL1_SENSOR_MAGNET_EXITS:
                {
                    printf("Next robot detected moving, park\n\r");
                    uint8_t mod[] = {'p', 0x00};
                    SPI1Write(mod,ARRAY_SIZE(mod));
                    uint8_t mod2[] = {'e', 0x00};
                    SPI2Write(mod2,ARRAY_SIZE(mod2));
                    nextState = Parking;
                    Timer5Init();
                    PWM1DutyCycle(3);
                }
                break;
            }
        }
        break;

        case Parking:
        {
            switch (ThisEvent.EventType){
                case ES_TIMEOUT:
                {
                    printf("Entering waiting mode\n\r");
                    uint8_t mod2[] = {'w', 0x00};
                    SPI1Write(mod2,ARRAY_SIZE(mod2));
                    nextState = Waiting;
                }
                break;
            }
        }
        break;

        case HardwareTesting:
        {
            switch (ThisEvent.EventType)
            {
                case ES_NEW_KEY:   // announce
                {
                    if (ThisEvent.EventParam == 'd') {
                        PWM1DutyCycle(12);
                    } else if (ThisEvent.EventParam == 'f') {
                        PWM1DutyCycle(11);

                    } else if (ThisEvent.EventParam == 'g') {
                        PWM1DutyCycle(3);
                        
                    } else if (ThisEvent.EventParam == 'i') {
                        LATAbits.LATA3 = 1;
                        LATAbits.LATA4 = 1;
                        
                    } else if (ThisEvent.EventParam == 'o') {
                        LATAbits.LATA3 = 0;
                        LATAbits.LATA4 = 0;

                    } else if (ThisEvent.EventParam == 'y') {
                        printf("Hall IO = %d\n\r", PORTBbits.RB4);
                        printf("Team Switch = %d\n\r", PORTAbits.RA2);
                        printf("Position Switch A = %d\n\r", PORTBbits.RB9);
                        printf("Position Switch B = %d\n\r", PORTBbits.RB12);

                    } else if (ThisEvent.EventParam == 'z') {
                        printf("z recieved\n\r");
                        uint8_t mod2[] = {0xF0, 0x00};
                        SPI2Write(mod2,ARRAY_SIZE(mod2));

                    } else if (ThisEvent.EventParam == 'x') {
                        printf("z recieved\n\r");
                        uint8_t mod2[] = {'w', 0x00};
                        SPI2Write(mod2,ARRAY_SIZE(mod2));

                    } else if (ThisEvent.EventParam == 'c') {
                        printf("z recieved\n\r");
                        uint8_t mod2[] = {'p', 0x00};
                        SPI2Write(mod2,ARRAY_SIZE(mod2));

                    } else if (ThisEvent.EventParam == 'v') {
                        printf("v recieved\n\r");
                        uint8_t mod2[] = {'w', 0x00};
                        printf("First: %d\n\rSecond: %d\n\rSize: %d\n\r",SPI2_BUFFER[0],SPI2_BUFFER[1],SPI2_BUFFER_SIZE);
                    }

                    printf("ES_NEW_KEY received with -> %c <- in Service 0\r\n",
                        (char)ThisEvent.EventParam);



                }
                break;

                case ES_HALL1_SENSOR_MAGNET_ENTERS:
                {
                    printf("Hall sensor 1 detected magnet entering!\n\r");
                    //PWM1DutyCycle(12);
                }
                break;

                case ES_HALL1_SENSOR_MAGNET_EXITS:
                {
                    printf("Hall sensor 1 detected magnet leaving!\n\r");
                }
                break;

                case ES_HALL2_SENSOR_MAGNET_ENTERS:
                {
                    printf("Hall sensor 2 detected magnet entering!\n\r");
                   // PWM1DutyCycle(3);
                }
                break;

                case ES_HALL2_SENSOR_MAGNET_EXITS:
                {
                    printf("Hall sensor 2 detected magnet leaving!\n\r");
                }
                break;

                case ES_PRINTOUT:
                {
                    printf("Timer 5 timed out\n\r");
                }
                break;

                case ES_SPI2_COMPLETE:
                {
                    SPI2Read(SPI2_BUFFER, &SPI2_BUFFER_SIZE);
                    printf("SPI BUFFER2 reports: %d\n\r", SPI2_BUFFER[0]);

                }
                break;

                case ES_LOC_REQUEST_TX:
                {
                    printf("LOC node requests data TX\n\r");

                }
                break;

                default:
                {}
                 break;
                 
            }
            
        }
        break;
    }
  

    currentState = nextState;
    
    return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

/*
#define LED LATBbits.LATB6
static void InitLED(void)
{
  LED = 0; //start with it off
  TRISBbits.TRISB6 = 0; // set RB6 as an output
}

static void BlinkLED(void)
{
  // toggle state of LED
  LED = ~LED;
}

#ifdef TEST_INT_POST
// for testing posting from interrupts.
// Intializes TMR2 to gerenate an interrupt at 100ms
static void InitTMR2(void)
{
  // turn timer off
  T2CONbits.ON = 0;
  // Use internal peripheral clock
  T2CONbits.TCS = 0;
  // setup for 16 bit mode
  T2CONbits.T32 = 0;
  // set prescale to 1:1
  T2CONbits.TCKPS = 0;
  // load period value
  PR2 = 2000-1; // creates a 100ms period with a 20MHz peripheral clock
  // set priority
  IPC2bits.T2IP = 2;
  // clear interrupt flag
  IFS0bits.T2IF = 0;
  // enable the timer interrupt
  IEC0bits.T2IE = 1;
}

// Clears and Starts TMR2
static void StartTMR2(void)
{
  // clear timer
  TMR2 = 0;
  // start timer
  LATBbits.LATB14 = 0;
  T2CONbits.ON = 1;
}

void __ISR(_TIMER_2_VECTOR, IPL2AUTO) Timer2ISR(void)
{
  // clear flag
  IFS0bits.T2IF = 0;
  // post event
  static ES_Event_t interruptEvent = {ES_SHORT_TIMEOUT, 0};
  PostTestHarnessService0(interruptEvent);
  
  // stop timer
  T2CONbits.ON = 0;
  return;
}
#endif
 * 
 * */

void IOsetupt() {
        
    
    // Switch to determine Team A or Team B
    //ANSELAbits.ANSA2; A2 does not have analog capabilities.
    TRISAbits.TRISA2 = 1;
    
    
    // Switch A for Starting position
    //ANSELBbits.ANSB9; //RB9 does not have analog capabilities
    TRISBbits.TRISB9 = 1;
    
    // Switch B for Starting position
    ANSELBbits.ANSB12 = 0; //RB12 does  have analog capabilities
    TRISBbits.TRISB12 = 1;
    
    // Green led
    //ANSELAbits.ANSA3; A3 does not have analog capabilities.
    TRISAbits.TRISA3 = 0;
    LATAbits.LATA3 = 0;
    
    // Red led
    //ANSELAbits.ANSA4; //A4 does not have analog capabilities.
    TRISAbits.TRISA4 = 0;
    LATAbits.LATA4 = 0;
    
    
    // Hall sensor 1
    //ANSELBbits.ANSB4 = 0;  RB4 does not have analog capabilities
    TRISBbits.TRISB4 = 1;  
    
    // IO line 
    ANSELAbits.ANSA1 = 0;
    TRISAbits.TRISA1 = 1;
   
    IFS1CLR = _IFS1_CNBIF_MASK;
    IFS1CLR = _IFS1_CNAIF_MASK;
    CNCONBbits.ON = 0;
    CNCONAbits.ON = 0;
    CNENBbits.CNIEB4 = 1;
    CNENAbits.CNIEA1 = 1;
    //CNENAbits.CNIEA3 = 1;
    //CNENAbits.CNIEA2 = 1;
    
    IPC8bits.CNIP = 7;
    IPC8bits.CNIS = 3;
    INTCONbits.MVEC = 1;

    IEC1SET = _IEC1_CNBIE_MASK;
    IEC1SET = _IEC1_CNAIE_MASK;
    
    CNCONBbits.ON = 1;
    CNCONAbits.ON = 1;
   
    __builtin_enable_interrupts();
    
    
}

void __ISR(_CHANGE_NOTICE_VECTOR, IPL7SOFT) IOISR(void) {
    
    if (CNSTATBbits.CNSTATB4) {
        uint16_t halVal = PORTBbits.RB4;
        if (halVal == 0){
            ES_Event_t SPI_Event;
            SPI_Event.EventType = ES_HALL1_SENSOR_MAGNET_ENTERS;
            PostGamePlay(SPI_Event);  
        } else if (halVal == 1) {
            ES_Event_t SPI_Event;
            SPI_Event.EventType = ES_HALL1_SENSOR_MAGNET_EXITS;
            PostGamePlay(SPI_Event); 
        } else {
            printf("ERROR in hall sensor 1 ISR\n\r");
        }
        
        
    }

    
    if (CNSTATAbits.CNSTATA1) {
        //printf("IO line changed\n\r");
        uint16_t pin3 = PORTAbits.RA1;
        /*
        
        if (pin3 == 1) {
            ES_Event_t SPI_Event;
            SPI_Event.EventType = ES_LOC_REQUEST_TX;
            PostGamePlay(SPI_Event);  
        }
        */
        
        
        
        ES_Event_t IO_Event;
        IO_Event.EventType = ES_IR_REQUEST_TX;
        PostGamePlay(IO_Event); 
        
    }
    IFS1CLR = _IFS1_CNBIF_MASK;
    IFS1CLR = _IFS1_CNAIF_MASK;
    
}


void __ISR(_TIMER_5_VECTOR, IPL7SOFT) TIMER_5_ISR(void) {
    //LATAbits.LATA4 = 1;
    //ES_Event_t Timer5_Event;
    //Timer5_Event.EventType = ES_PRINTOUT;
    //PostGamePlay(Timer5_Event);  
    static volatile uint8_t rollover = 0;
    
    
    IFS0CLR = _IFS0_T5IF_MASK;
    if (rollover < 6) {
        rollover++;
    } else {
        ES_Event_t Timer5_event;
        Timer5_event.EventType = ES_TIMEOUT;
        PostGamePlay(Timer5_event); 
        T5CONbits.ON = 0;
        rollover = 0;
    }
    
    
    

}


/*
void __ISR(_SPI_2_VECTOR, IPL7SOFT) SPI2Handler(void) {
    // Debug counter to count how many times the ISR is entered.
    counter++;
    
    
    //uint8_t flagRecieve = IFS1bits.SPI2RXIF;
    //uint8_t flagTransmit = IFS1bits.SPI2TXIF;
    //uint8_t flagError = IFS1bits.SPI2EIF;
    
    // Read the command in to store it in a stable location
    uint8_t command = SPI2BUF;
    
    // Clear the interrupts flags
    IFS1CLR = _IFS1_SPI2RXIF_MASK;
    IFS1CLR = _IFS1_SPI2TXIF_MASK;
    
    // If command is 'q', send back 0x00 and turn the led on
    // Post event so the run function will printf the command.
    if (command == 'q') {
        LATAbits.LATA4 = 1; 
        SPI2BUF = 0x00;
        ES_Event_t SPI_Event;
        SPI_Event.EventType = ES_COMMAND;
        SPI_Event.EventParam = 'O';
        PostFollower2Node(SPI_Event);  
        
    // If command is 'w', send back 0x00 and turn off the led  
    // Post event so the run function will printf the command.
    } else if (command == 'w') {
        LATAbits.LATA4 = 0;
        SPI2BUF = 0x00;
        ES_Event_t SPI_Event;
        SPI_Event.EventType = ES_COMMAND;
        SPI_Event.EventParam = 'o';
        PostFollower2Node(SPI_Event);
        
    // If the command is 'p', depending of the state of the input pin send
    // back relevant data. Post event so the run function will printf the command.
    } else if (command == 'p') {
        // If input pin is high, send back 0x07
        if (PORTBbits.RB4 == 1) {
            SPI2BUF = 7;    
        // If the input pin is low, send back 0x05
        } else {
            SPI2BUF = 5;  
        }
        // Post Event
        ES_Event_t SPI_Event;
        SPI_Event.EventType = ES_COMMAND;
        SPI_Event.EventParam = 'q';
        PostFollower2Node(SPI_Event); 
    }
}
 * 
 * */




/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

