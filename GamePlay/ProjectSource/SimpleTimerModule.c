
#include "SimpleTimerModule.h"

void Timer2Init() {
    printf("Hello, timer 2 has been initiated\n\r");
    
    // Time Setup
    // Turn Timer off
    T2CONbits.ON = 0;
    // Select internal clock source
    T2CONbits.TCS = 0;
    // Select prescale value
    T2CONbits.TCKPS = 0b100;
    // Clear timer
    TMR2 = 0;
    // Set period of timer to max - 5 tau of motor, motor const = 241us
    PR2 = 3012;
    // Clear Timer interrupt flags
    IFS0CLR = _IFS0_T2IF_MASK;
    // Set Timer Priority
    IPC2bits.T2IP = 6;
    // Set Timer Subpriority
    IPC2bits.T2IS = 3;  
    // Enable Interrupts locally
    //IEC0SET = _IEC0_T2IE_MASK;
    // Enable interrupts globally
    //__builtin_enable_interrupts();
    // Set multivector interrupts
    INTCONbits.MVEC = 1;
    // Turn Timer On
    T2CONbits.ON = 1;
}


void Timer3Init() {
    printf("Hello, timer 3 has been initiated\n\r");
    
    // Time Setup
    // Turn Timer off
    T3CONbits.ON = 0;
    // Select internal clock source
    T3CONbits.TCS = 0;
    // Select prescale value
    T3CONbits.TCKPS = 0b011;
    // Clear timer
    TMR3 = 0;
    // Set period of timer to max
    PR3 = 0xFFFF;
    // Clear Timer interrupt flags
    IFS0CLR = _IFS0_T3IF_MASK;
    // Set Timer Priority
    IPC3bits.T3IP = 6;
    // Set Timer Subpriority
    IPC3bits.T3IS = 3;  
    // Enable Interrupts locally
    IEC0SET = _IEC0_T3IE_MASK;
    // Enable interrupts globally
    __builtin_enable_interrupts();
    // Set multivector interrupts
    INTCONbits.MVEC = 1;
    // Turn Timer On
    T3CONbits.ON = 1;
}


void Timer4Init() {
    printf("Hello, timer 4 has been initiated\n\r");
    
    // Time Setup
    // Turn Timer off
    T4CONbits.ON = 0;
    // Select internal clock source
    T4CONbits.TCS = 0;
    // Select prescale value
    T4CONbits.TCKPS = 0b111;
    // Clear timer
    TMR4 = 0;
    // Set period of timer to max - 5 tau of motor, motor const = 241us
    PR4 = 7812;
    // Clear Timer interrupt flags
    IFS0CLR = _IFS0_T4IF_MASK;
    // Set Timer Priority
    IPC4bits.T4IP = 6;
    // Set Timer Subpriority
    IPC4bits.T4IS = 3;  
    // Enable Interrupts locally
    IEC0SET = _IEC0_T4IE_MASK;
    // Enable interrupts globally
    __builtin_enable_interrupts();
    // Set multivector interrupts
    INTCONbits.MVEC = 1;
    // Turn Timer On
    T4CONbits.ON = 1;
}

void Timer5Init() {
    printf("Hello, timer 5 has been initiated\n\r");
    
    // Time Setup
    // Turn Timer off
    T5CONbits.ON = 0;
    // Select internal clock source
    T5CONbits.TCS = 0;
    // Select prescale value
    T5CONbits.TCKPS = 0b111;
    // Clear timer
    TMR5 = 0;
    // Se
    PR5 = 65535;
    // Clear Timer interrupt flags
    IFS0CLR = _IFS0_T5IF_MASK;
    // Set Timer Priority
    IPC5bits.T5IP = 7;
    // Set Timer Subpriority
    IPC5bits.T5IS = 3;  
    // Enable Interrupts locally
    IEC0SET = _IEC0_T5IE_MASK;
    // Enable interrupts globally
    __builtin_enable_interrupts();
    // Set multivector interrupts
    INTCONbits.MVEC = 1;
    // Turn Timer On
    T5CONbits.ON = 1;
}