#include "../ProjectHeaders/spiHAL.h"
#include "../ProjectHeaders/gamePlay.h"

// Module level variables
static char SPI2_BUF[50];
static volatile uint8_t SPI2_pos = 0;
static volatile uint8_t SPI2_size = 0;

static char SPI1_BUF[50];
static volatile uint8_t SPI1_pos = 0;
static volatile uint8_t SPI1_size = 0;


void SPI1Setup() {
    // Turn SPI2 Off
      SPI1CONbits.ON = 0;
      
      // Ensure all pins are in digital mode.
      //ANSELBbits.ANSB5 = 0; // RB10 does not have analog capabilities
      //ANSELBbits.ANSB7 = 0; //RB11 does not have analog capabilities
      //ANSELBbits.ANSB8 = 0;
      ANSELBbits.ANSB14 = 0;
      
      // Map slave select 1 input to RB7/pin16 and set RB7 as output.
      LATBbits.LATB7 = 1;
      TRISBbits.TRISB7 = 0;
      RPB7R = 0b0011;
      
      
      // Map SDO1 to RB5/pin14 and set RB11 to output
      TRISBbits.TRISB5 = 0;
      RPB5R = 0b0011;
      
      // Map SDI1 to RB8/pin 17 and set RB8 to input
      TRISBbits.TRISB8 = 1;
      SDI1R = 0b0100;
      
      // SCK1 will always be on Pin25/RB14
      TRISBbits.TRISB14 = 0;
      
      // Clear the buffer by reading from it
      uint8_t clear = SPI1BUF;
      
      // Do not use enhanced buffer mode
      SPI1CONbits.ENHBUF = 0;
      
      // Set baud rate to 100,000Hz with BRG value of 99
      SPI1BRG = 99;
      
      // CLear recieve overflow flag
      SPI1STATbits.SPIROV = 0;
      
      SPI1CONbits.MSSEN = 1; // enable slave select SPI
      // Set SPI1 in the master setup
      SPI1CONbits.MSTEN = 1;
      
      // SS active low with FRMPOL = 0
      SPI1CONbits.FRMPOL = 0;
      // 2nd edge for data with CKE = 0
      SPI1CONbits.CKE = 0;
      // CKP idle high with CKP = 1
      SPI1CONbits.CKP = 1;
      // Mode bit selection: 8bit with MODE16 and MODE32 = 0
      SPI1CONbits.MODE16 = 0;
      SPI1CONbits.MODE32 = 0;
      
      // Enable Interrupts with IEC1SET 
      IEC1SET = _IEC1_SPI1RXIE_MASK;
      // Set SPI2 priority at 7
      IPC7bits.SPI1IP = 7;
      // Set SPI2 sub-priority at 3
      IPC7bits.SPI1IS = 3;
      // Enable interrupts globally
      __builtin_enable_interrupts();
              
      // Turn SPI On stupid
      SPI1CONbits.ON = 1;
}

void __ISR(_SPI_1_VECTOR, IPL7SOFT) SPI1Handler(void) {
    //LATAbits.LATA4 = 1;
    //printf("Hello");
    //uint8_t flagRecieve = IFS1bits.SPI2RXIF;
    //uint8_t flagTransmit = IFS1bits.SPI2TXIF;
    //uint8_t flagError = IFS1bits.SPI2EIF;
    
    // Read the command in to store it in a stable location
    uint8_t command = SPI1BUF;
    
    // Clear the interrupts flags
    IFS1CLR = _IFS1_SPI1RXIF_MASK;
    IFS1CLR = _IFS1_SPI1TXIF_MASK;
    
    if (SPI1_pos < SPI1_size) {
        SPI1BUF = SPI1_BUF[SPI1_pos++];
    } else {
  
    }
        
}


void SPI1Write(char * bytes, uint8_t size) {
    memcpy(SPI1_BUF, bytes, size);
    SPI1_size = size;
    SPI1_pos = 0;
    
    SPI1BUF = SPI1_BUF[SPI1_pos++];
}

void SPI1Read(char * bytes, uint8_t * size) {
    
    //printf("DELAY\n\r");
    memcpy(bytes, SPI1_BUF, SPI1_size);
    *size = SPI1_size;

}


void SPI2Setup() {
    // Turn SPI2 Off
      SPI2CONbits.ON = 0;
      
      // Ensure all pins are in digital mode.
      //ANSELBbits.ANSB10 = 0; // RB10 does not have analog capabilities
      //ANSELBbits.ANSB11 = 0; //RB11 does not have analog capabilities
      ANSELBbits.ANSB13 = 0;
      ANSELBbits.ANSB15 = 0;
      
      // Map slave select input to RB10/pin21 and set RB10 as output.
      LATBbits.LATB10 = 1;
      TRISBbits.TRISB10 = 0;
      //SS2R = 0b0011;
      RPB10R = 0b0100;
      
      
      // Map SDO2 to RB11/pin22 and set RB11 to output
      TRISBbits.TRISB11 = 0;
      RPB11R = 0b0100;
      
      // Map SDI2 to RB13/pin 24 and set RB13 to input
      TRISBbits.TRISB13 = 1;
      SDI2R = 0b0011;
      
      // SCK2 will always be on Pin26/RB15
      TRISBbits.TRISB15 = 0;
      
      // Clear the buffer by reading from it
      uint8_t clear = SPI2BUF;
      
      // Do not use enhanced buffer mode
      SPI2CONbits.ENHBUF = 0;
      
      // Set baud rate to 100,000Hz with BRG value of 99
      SPI2BRG = 99;
      
      // CLear recieve overflow flag
      SPI2STATbits.SPIROV = 0;
      
      SPI2CONbits.MSSEN = 1; // enable slave select SPI
      // Set SPI2 in the master setup
      SPI2CONbits.MSTEN = 1;
      
      // SS active low with FRMPOL = 0
      SPI2CONbits.FRMPOL = 0;
      // 2nd edge for data with CKE = 0
      SPI2CONbits.CKE = 0;
      // CKP idle high with CKP = 1
      SPI2CONbits.CKP = 1;
      // Mode bit selection: 8bit with MODE16 and MODE32 = 0
      SPI2CONbits.MODE16 = 0;
      SPI2CONbits.MODE32 = 0;
      
      // Enable Interrupts with IEC1SET 
      IEC1SET = _IEC1_SPI2RXIE_MASK;
      // Set SPI2 priority at 7
      IPC9bits.SPI2IP = 7;
      // Set SPI2 sub-priority at 3
      IPC9bits.SPI2IS = 3;
      // Enable interrupts globally
      __builtin_enable_interrupts();
              
      // Turn SPI On stupid
      SPI2CONbits.ON = 1;
}



void __ISR(_SPI_2_VECTOR, IPL7SOFT) SPI2Handler(void) {
    //LATAbits.LATA4 = 1;
    //printf("Hello");
    //uint8_t flagRecieve = IFS1bits.SPI2RXIF;
    //uint8_t flagTransmit = IFS1bits.SPI2TXIF;
    //uint8_t flagError = IFS1bits.SPI2EIF;
    
    // Read the command in to store it in a stable location
    uint8_t command = SPI2BUF;
    SPI2_BUF[SPI2_pos-1] = command;
    
    // Clear the interrupts flags
    IFS1CLR = _IFS1_SPI2RXIF_MASK;
    IFS1CLR = _IFS1_SPI2TXIF_MASK;
    
    if (SPI2_pos < SPI2_size) {
        SPI2BUF = SPI2_BUF[SPI2_pos++];
    } else {
        ES_Event_t SPI_Event;
        SPI_Event.EventType = ES_SPI2_COMPLETE;
        PostGamePlay(SPI_Event);
    }
        
}


void SPI2Write(char * bytes, uint8_t size) {
    memcpy(SPI2_BUF, bytes, size);
    SPI2_size = size;
    SPI2_pos = 0;
    
    SPI2BUF = SPI2_BUF[SPI2_pos++];
}

void SPI2Read(char * bytes, uint8_t * size) {
    LATAbits.LATA4 = 1;
    //printf("DELAY\n\r");
    memcpy(bytes, SPI2_BUF, SPI2_size);
    *size = SPI2_size;
    LATAbits.LATA4 = 0;
}