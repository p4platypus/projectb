#include "SimplePWM.h"

void PWM1Init() {
    // Pin setup, expand to allow user to choose pin #
    // Pin 15 cannot be analog
    //ANSELBbits.ANSB6 = 0
    // Set pin 15 as output
    TRISBbits.TRISB6 = 0;
    // Turn off OC1
    OC4CONbits.ON = 0;
    // Set PWM period
    PR2 = 25000;
    // Set Duty Cycle
    OC4RS = 2500;
    // Set Initial Duty Cycle
    OC4R = 2500;
    // Enable interrutps
    //IFS0SET = _IFS0_OC4IF_MASK;
    // Timer 2 as Source
    OC4CONbits.OCTSEL = 0;
    // Set OC1 to PWM m ode, disabled fault pin
    OC4CONbits.OCM = 0b110;
    // Map OC1 to pin 16
    RPB6R = 0b0101;
    // Turn on PWM
    OC4CONbits.ON = 1;
    
    return;
}


void PWM1DutyCycle(uint16_t DC) {
    //printf("DC: %d\n\r", DC);
    
    // Bound PWM
    if (DC > 100) {
        DC = 100;
    } else if (DC < 0) {
        DC = 0;
    }
    uint16_t newOC4RS = (PR2 + 1)*DC/100;
    OC4RS = newOC4RS;
    //printf("OC: %d\n\r", newOC1RS);
    return;
}
