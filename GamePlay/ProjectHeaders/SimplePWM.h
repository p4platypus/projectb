/* 
 * File:   SimplePWM.h
 * Author: Turiae
 *
 * Created on February 6, 2021, 9:17 AM
 */

#ifndef SIMPLEPWM_H
#define	SIMPLEPWM_H

#include <sys/attribs.h> // for ISR macors
#include <xc.h>
#include <proc/p32mx170f256b.h>
#include <stdio.h>

// Function Declarations
void PWM1Init();
void PWM1DutyCycle(uint16_t DC);

#endif	/* SIMPLEPWM_H */

