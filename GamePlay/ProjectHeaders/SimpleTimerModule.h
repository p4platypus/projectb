/* 
 * File:   SimpleTimerModule.h
 * Author: Turiae
 *
 * Created on February 6, 2021, 8:56 AM
 */

#ifndef SIMPLETIMERMODULE_H
#define	SIMPLETIMERMODULE_H

#include <sys/attribs.h> // for ISR macors
#include <xc.h>
#include <proc/p32mx170f256b.h>
#include <stdio.h>


// Function declarations
void Timer2Init();
void Timer3Init();
void Timer4Init();
void Timer5Init();

#endif	/* SIMPLETIMERMODULE_H */

