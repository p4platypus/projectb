/* 
 * File:   gamePlay.h
 * Author: Turiae
 *
 * Created on February 21, 2021, 9:29 PM
 */

#ifndef GAMEPLAY_H
#define	GAMEPLAY_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Events.h"
#include "ES_Port.h"                // needed for definition of REENTRANT

typedef enum
{
  InitGameElements, Waiting, Relay, Parking, HardwareTesting, Transmission
}GameState_t;


// Public Function Prototypes

bool InitGamePlay(uint8_t Priority);
bool PostGamePlay(ES_Event_t ThisEvent);
ES_Event_t RunGamePlay(ES_Event_t ThisEvent);

#endif /* ServTemplate_H */

