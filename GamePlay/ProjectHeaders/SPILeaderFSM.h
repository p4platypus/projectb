/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef FSMSPILeader_H
#define FSMSPILeader_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitPState, Waiting, Printing, TURNING_LED_1_ON, TURNING_LED_1_OFF, 
          TURNING_LED_2_ON, TURNING_LED_2_OFF
}SPILeaderState_t;

// Public Function Prototypes

bool InitSPILeaderFSM(uint8_t Priority);
bool PostSPILeaderFSM(ES_Event_t ThisEvent);
ES_Event_t RunSPILeaderFSM(ES_Event_t ThisEvent);
SPILeaderState_t QuerySPILeaderFSM(void);

#endif /* FSMSPILeader_H */

