/* 
 * File:   spiHAL.h
 * Author: Turiae
 *
 * Created on February 24, 2021, 11:51 PM
 */

#ifndef SPIHAL_H
#define	SPIHAL_H

#include <stdint.h>
#include <stdbool.h>

#include "ES_Events.h"
#include "ES_Port.h"  

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Port.h"

#include <stdio.h>
#include <string.h> 

// Hardware
#include <xc.h>
#include <proc/p32mx170f256b.h>
#include <sys/attribs.h> // for ISR macors

// needed for definition of REENTRANT
// Public Function Prototypes

void SPI2Setup();
void SPI2Write(char * bytes, uint8_t size);
void SPI2Read(char * bytes, uint8_t * size);

void SPI1Setup();
void SPI1Write(char * bytes, uint8_t size);
void SPI1Read(char * bytes, uint8_t * size);

#endif /* ServTemplate_H */

