#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# Justu8g2POC configuration
CND_ARTIFACT_DIR_Justu8g2POC=dist/Justu8g2POC/production
CND_ARTIFACT_NAME_Justu8g2POC=IR.production.hex
CND_ARTIFACT_PATH_Justu8g2POC=dist/Justu8g2POC/production/IR.production.hex
CND_PACKAGE_DIR_Justu8g2POC=${CND_DISTDIR}/Justu8g2POC/package
CND_PACKAGE_NAME_Justu8g2POC=ir.tar
CND_PACKAGE_PATH_Justu8g2POC=${CND_DISTDIR}/Justu8g2POC/package/ir.tar
# FrameworkWith_u8g2 configuration
CND_ARTIFACT_DIR_FrameworkWith_u8g2=dist/FrameworkWith_u8g2/production
CND_ARTIFACT_NAME_FrameworkWith_u8g2=IR.production.hex
CND_ARTIFACT_PATH_FrameworkWith_u8g2=dist/FrameworkWith_u8g2/production/IR.production.hex
CND_PACKAGE_DIR_FrameworkWith_u8g2=${CND_DISTDIR}/FrameworkWith_u8g2/package
CND_PACKAGE_NAME_FrameworkWith_u8g2=ir.tar
CND_PACKAGE_PATH_FrameworkWith_u8g2=${CND_DISTDIR}/FrameworkWith_u8g2/package/ir.tar
