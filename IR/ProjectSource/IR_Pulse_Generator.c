/* 
 * File:   IR_Pulse_Generator.c
 * Author: Vivienne
 *
 * Created on February 21, 2021, 11:52 AM
 */

/*---------------------------- Includes ---------------------------*/

// hardware
#include <sys/attribs.h>
#include "ES_Port.h"

// this module
#include "IR_Pulse_Generator.h"

/*---------------------------- Module Functions ---------------------------*/




/*---------------------------- Module Variables ---------------------------*/

// LED toggle
static uint8_t LED_status = 0;

// timer periods for output compare
#define timer_ticks_300us 46 // with a 1:64 prescale on a 20MHz clock, 47 ticks corresponds to ~150 us (half cycle)
#define timer_ticks_700us 109 // similarly ^
#define timer_ticks_500us 78 // similarly ^
#define timer_ticks_1100us 172 // similarly ^

/*---------------------------- Main ---------------------------*/

/*
 * 
 */
#define TEST
#ifdef TEST
int main(void) {
    _HW_PIC32Init();
    LED_pin_setup();
    printf("In test harness. LED HW set up \n\r");
    IR_OC_HW_init();
    printf("Output compare HW set up - 9:22 \n\r");
    while(true)
    { 
    /*
    if(IsNewKeyReady())
    {
        GetNewKey();
        LATBbits.LATB15 = !LATBbits.LATB15;
    }
    */
    }
        
    return (EXIT_SUCCESS);
}
#endif // end if on test harness


/*---------------------------- Module Functions ---------------------------*/

void LED_pin_setup(void)
{
    ANSELBbits.ANSB15 = 0; // Set to digital - already digital
    TRISBbits.TRISB15 = 0; // Set to output
    LATBbits.LATB15 = 0; // Set low
}

void LED_on(void)
{
    LATBbits.LATB15 = 1;
}

void LED_off(void)
{
    LATBbits.LATB15 = 0;
}

void IR_OC_HW_init(void) // uses OC1 and TMR3
{
// HW INIT FOR OUTPUT COMPARE // Single compare output mode (https://ww1.microchip.com/downloads/en/DeviceDoc/61111E.pdf, p. 9)
          
          // Use pin 26 (RB15)
          ANSELBbits.ANSB15 = 0; // Set to digital - already digital
          TRISBbits.TRISB15 = 0; // Set to output
          RPB15R = 0b0101; // using pin 26 (RB15) for output compare
          
          // Enable output compare module
          OC1CONbits.ON = 0b0; // Disable output compare
          OC1CONbits.OCM = 0b011; // Single compare mode
          OC1CONbits.OCTSEL = 1; // Use timer 3
          
          // Select and set up timer / period
          T3CONbits.ON = 0;         // Clear on bit
          T3CONbits.TCS = 0;       // Select internal PBCLK
          T3CONbits.TCKPS = 0b110; // 1:64 prescale
          TMR3 = 0; // Load / clear the timer reg
          OC1R = 1;
          PR3 = timer_ticks_300us; // Reset for next toggle. Add one to ensure OC occurs BEFORE timer reset
          
          // Set up interrupts
          T3CONbits.ON = 1; // timer on
          // IFS0CLR = _IFS0_T3IF_MASK;      // Clear the interrupt flag
          IFS0CLR = _IFS0_OC1IF_MASK;   // Clear the interrupt flag
          IPC3bits.T3IP = 6; // Configure timer interrupt priority (pick 6 though 7 is maximum - per "Handling the Possibilities" handout)
          IPC3bits.T3IS = 3; // Configure interrupt sub-priority (3 is maximum))
          
          INTCONbits.MVEC = 1; // Set up for multiple interrupt vectors
          __builtin_enable_interrupts(); // Enable global interrupt
          IEC0SET = _IEC0_OC1IE_MASK; // Enable local interrupt
          
          OC1CONbits.ON = 0b1; // Enable output compare
}

void __ISR(_OUTPUT_COMPARE_1_VECTOR, IPL6SOFT) IR_OC_Handler(void)
{
    IFS0CLR = _IFS0_OC1IF_MASK;
    TMR3 = 0; // Reset timer for next count up
}