/****************************************************************************
 Module
   IR_FSM.c

 Revision
   1.0.1

 Description
 IR FSM for ME218B Project (Team 7)

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/15/12 11:12 jec      revisions for Gen2 framework
 11/07/11 11:26 jec      made the queue static
 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "IR_FSM.h"
#include <xc.h>
#include <sys/attribs.h>

/*----------------------------- Module Defines ----------------------------*/

/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this machine.They should be functions
   relevant to the behavior of this state machine
*/

/*---------------------------- Module Variables ---------------------------*/
// everybody needs a state variable, you may need others as well.
// type of state variable should match htat of enum in header file
static IR_State_t CurrentState;

// with the introduction of Gen2, we need a module level Priority var as well
static uint8_t MyPriority;

// timer periods for output compare
#define timer_ticks_300us 47 // with a 1:64 prescale on a 20MHz clock, 47 ticks corresponds to ~150 us (half cycle)
#define timer_ticks_700us 109 // similarly ^
#define timer_ticks_500us 78 // similarly ^
#define timer_ticks_1100us 172 // similarly ^
/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateFSM

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition and does any
     other required initialization for this state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 18:55
****************************************************************************/
bool Init_IR_FSM(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  // put us into the Initial PseudoState
  CurrentState = InitPState;
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateFSM

 Parameters
     EF_Event_t ThisEvent , the event to post to the queue

 Returns
     boolean False if the Enqueue operation failed, True otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool Post_IR_FSM(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateFSM

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event_t, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes
   uses nested switch/case to implement the machine.
 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t Run_IR_FSM(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors

  switch (CurrentState)
  {
    case InitPState:        // If current state is initial Psedudo State
    {
      if (ThisEvent.EventType == ES_INIT)    // only respond to ES_Init
      {
          // HW INIT FOR OUTPUT COMPARE // Single compare output mode (https://ww1.microchip.com/downloads/en/DeviceDoc/61111E.pdf, p. 9)
          
          // Use pin 26 (RB15)
          ANSELBbits.ANSB15 = 0; // Set to digital - already digital
          TRISBbits.TRISB15 = 0; // Set to output
          RPB15R = 0b0101; // using pin 26 (RB15) for output compare
          
          // Enable output compare module
          
          OC1CONbits.ON = 0b1; // Enable output compare
          OC1CONbits.OCM = 0b011; // Single compare mode
          OC1CONbits.OCTSEL = 1; // Use timer 3
          
          // Select and set up timer / period
          T3CONbits.ON = 0;         // Clear on bit
          T3CONbits.TCS = 0;       // Select internal PBCLK
          T3CONbits.TCKPS = 0b110; // 1:64 prescale
          TMR3 = 0; // Load / clear the timer reg
          OC1R = timer_ticks_300us;
          PR3 = 0xFFFF; // Maximize period to minimize rollovers
          
          // Set up interrupts
          T3CONbits.ON = 1; // timer on
          IFS0CLR = _IFS0_T3IF_MASK;      // Clear the interrupt flag
          IPC3bits.T3IP = 6; // Configure timer interrupt priority (pick 6 though 7 is maximum - per "Handling the Possibilities" handout)
          IPC3bits.T3IS = 3; // Configure interrupt sub-priority (3 is maximum))
          
          // LATBbits.LATB15 = 0; // Set low (LED off)

        // now put the machine into the actual initial state
        CurrentState = IR_Waiting;
        printf("IR init complete. Moving into IR_Waiting \n\r");
      }
    }
    break;

    case IR_Waiting:        // If current state is state one
    {
      switch (ThisEvent.EventType)
      {
        case ES_NEW_KEY:  //If event is event one
        {   
          // printf("New key received; toggling pin 26 \n\r");  
          // LATBbits.LATB15 = !LATBbits.LATB15;  // Toggle pin on keystroke
          CurrentState = IR_Waiting;  //Decide what the next state will be
        }
        break;

        // repeat cases as required for relevant events
        default:
          ;
      }  // end switch on CurrentEvent
    }
    break;
    // repeat state pattern as required for other states
    default:
      ;
  }                                   // end switch on Current State
  return ReturnEvent;
}

/****************************************************************************
 Function
     QueryTemplateSM

 Parameters
     None

 Returns
     TemplateState_t The current state of the Template state machine

 Description
     returns the current state of the Template state machine
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:21
****************************************************************************/
IR_State_t Query_IR_FSM(void)
{
  return CurrentState;
}

/***************************************************************************
 private functions
 ***************************************************************************/
/*
// ISR now handled in "IR_Pulse_Generator.c"
void __ISR(_OUTPUT_COMPARE_1_VECTOR, IPL6SOFT) IR_OC_Handler(void)
{
    IFS0CLR = _IFS0_T3IF_MASK; // Clear flag
    LATBbits.LATB11 = 1; // Initialize at one to test // zero (not in ISR)
    
    lastControlEncoderPeriod_Ticks = lastEncoderPeriod_Ticks;
    lastRPM = per2RPM / lastControlEncoderPeriod_Ticks;
    lastControlRPM = lastRPM; // capture the RPM measured during the last control law
    
    RPMError = TargetRPM - lastRPM;
    SumError += RPMError;
    
    RequestedDuty = k_p * RPMError + k_I * SumError;          // control law
    
    if (RequestedDuty > 100){      // anti windup
        RequestedDuty = 100;
        SumError -= RPMError;
    }
    
    else if (RequestedDuty < 0){    // anti windup
        RequestedDuty = 0;
        SumError -= RPMError;
    }
    
     // OC1RS = 0x8000; // debugging - check that OC1RS is being set by this loop
    OC1RS = (uint32_t) ((RequestedDuty * PR2)/100);             // update the duty cycle
    
    LATBbits.LATB11 = 0; // end of execution time
    
    
}
*/