/****************************************************************************
 Module
   TestHarnessService0.c

 Revision
   1.0.1

 Description
   This is the first service for the Test Harness under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 10/26/17 18:26 jec     moves definition of ALL_BITS to ES_Port.h
 10/19/17 21:28 jec     meaningless change to test updating
 10/19/17 18:42 jec     removed referennces to driverlib and programmed the
                        ports directly
 08/21/17 21:44 jec     modified LED blink routine to only modify bit 3 so that
                        I can test the new new framework debugging lines on PF1-2
 08/16/17 14:13 jec      corrected ONE_SEC constant to match Tiva tick rate
 11/02/13 17:21 jec      added exercise of the event deferral/recall module
 08/05/13 20:33 jec      converted to test harness service
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// This module
#include "../ProjectHeaders/TestHarnessService0.h"

// debugging printf()
//#include "dbprintf.h"

// Hardware
#include <xc.h>
#include <proc/p32mx170f256b.h>
#include <sys/attribs.h> // for ISR macors

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_DeferRecall.h"
#include "ES_ShortTimer.h"
#include "ES_Port.h"

/*----------------------------- Module Defines ----------------------------*/
// these times assume a 10.000mS/tick timing
#define ONE_SEC 1000
#define HALF_SEC (ONE_SEC / 2)
#define TWO_SEC (ONE_SEC * 2)
#define FIVE_SEC (ONE_SEC * 5)

#define ENTER_POST     ((MyPriority<<3)|0)
#define ENTER_RUN      ((MyPriority<<3)|1)
#define ENTER_TIMEOUT  ((MyPriority<<3)|2)

#define TEST_INT_POST
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/

static void InitLED(void);
static void BlinkLED(void);
#ifdef TEST_INT_POST
static void InitTMR2(void);
static void StartTMR2(void);
#endif
/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
// add a deferral queue for up to 3 pending deferrals +1 to allow for overhead
static ES_Event_t DeferralQueue[3 + 1];

volatile static uint8_t counter;



/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTestHarnessService0

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitTestHarnessService0(uint8_t Priority)
{
    counter = 0;
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************
   in here you write your initialization code
   *******************************************/
  // initialize deferral queue for testing Deferal function
  //ES_InitDeferralQueueWith(DeferralQueue, ARRAY_SIZE(DeferralQueue));
  // initialize LED drive for testing/debug output

  // initialize the Short timer system for channel A
  //ES_ShortTimerInit(MyPriority, SHORT_TIMER_UNUSED);

  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTestHarnessService0

 Parameters
     EF_Event ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostTestHarnessService0(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTestHarnessService0

 Parameters
   ES_Event : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunTestHarnessService0(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  static char DeferredChar = '1';

#ifdef _INCLUDE_BYTE_DEBUG_
  _HW_ByteDebug_SetValueWithStrobe( ENTER_RUN );
#endif  
  switch (ThisEvent.EventType)
  {
    case ES_INIT:
    {
      puts("Service 00:");
      printf("\rES_INIT received in Service %d\r\n", MyPriority);
      
      for(int i = 0; i < 100000; i++) {}
      
      // Turn SPI2 Off
      SPI2CONbits.ON = 0;
      
      //ANSELBbits.ANSB10 = 0; // RB10 does not have analog capabilities
      //ANSELBbits.ANSB11 = 0; //RB11 does not have analog capabilities
      ANSELBbits.ANSB13 = 0;
      ANSELBbits.ANSB15 = 0;
      
      // Map slave select input to RB10/pin21
      TRISBbits.TRISB10 = 1;
      SS2R = 0b0011;
      
      // Map SDO2 to RB11/pin22
      TRISBbits.TRISB11 = 0;
      RPB11R = 0b0100;
      
      // Map SDI2 to RB13/pin 24
      TRISBbits.TRISB13 = 1;
      SDI2R = 0b0011;
      
      // SCK2 will always be on Pin26/RB15
      
      // Clear the buffer by reading from it? Is this necessary?
      uint8_t clear = SPI2BUF;
      
      SPI2CONbits.ENHBUF = 0;
      
      SPI2BRG = 99;
      
      // SPI overflow clear
 
      
      SPI2CONbits.MSTEN = 0;
      
      // SS active low
      SPI2CONbits.FRMPOL = 0;
      // 2nd edge for data
      SPI2CONbits.CKE = 0;
      // CKP idle high
      SPI2CONbits.CKP = 1;
      // Mode bit selection: 8bit
      SPI2CONbits.MODE16 = 0;
      SPI2CONbits.MODE32 = 0;
      
      // Enable Interrupts
      SPI2CONbits.SRXISEL = 0b01;
      
      IEC1SET = _IEC1_SPI2RXIE_MASK;
      IPC9bits.SPI2IP = 7;
      IPC9bits.SPI2IS = 3;
      __builtin_enable_interrupts();
              
      
      // Turn SPI On stupid
      SPI2CONbits.ON = 1;
     
              
      
      TRISAbits.TRISA4 = 0;
      LATAbits.LATA4 = 0;
      
      // Set up input for RB4
      // No analog for RB4
      TRISBbits.TRISB4 = 1;
    }
    break;

    case ES_NEW_KEY:   // announce
    {
      printf("ES_NEW_KEY received with -> %c <- in ServiceTEST 0\r\n",
          (char)ThisEvent.EventParam);
      if ('d' == ThisEvent.EventParam)
      {
          LATAbits.LATA4 = 1;
          SPI2BUF = 0b00111100;
          counter = 0;
      }
      if ('r' == ThisEvent.EventParam)
      {
          LATAbits.LATA4 = 0;
          SPI2BUF = 0xAA;
          counter = 0;
      }

    }
    break;
    
      case ES_COMMAND : 
      {
          if (ThisEvent.EventParam =='q') {
              printf("Follower2 says: Command received to query state of input pin.\n\r");
          } else if (ThisEvent.EventParam =='o') {
              printf("Follower2 says: Command received to turn off LED.\n\r");
          } else if (ThisEvent.EventParam =='O') {
              printf("Follower2 says: Command received to turn ON LED.\n\r");
          } else {
              printf("Follower2 says: Unknown command received.\n\r");
          }
          
      }
      break;
    
    default:
    {}
     break;
  }

  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

void __ISR(_SPI_2_VECTOR, IPL7SOFT) SPI2Handler(void) {
    counter++;
    
    
    uint8_t flagRecieve = IFS1bits.SPI2RXIF;
    uint8_t flagTransmit = IFS1bits.SPI2TXIF;
    uint8_t flagError = IFS1bits.SPI2EIF;
    
    
    uint8_t command = SPI2BUF;
    
    IFS1CLR = _IFS1_SPI2RXIF_MASK;
    IFS1CLR = _IFS1_SPI2TXIF_MASK;
    
    if(counter > 1) {
        flagRecieve = 0;
    }
    
    //printf("Recieve : %d\n\r Transmit : %d\n\r Error : %d\n\r", flagRecieve, flagTransmit, flagError);
    //IFS1CLR = _IEC1_SPI2TXIE_MASK;
    //IFS1CLR = _IEC1_SPI2EIF_MASK;
    //printf("Interrupt has been recieved for SPI2 Recieve\n\r");
    
    //printf("%d\n\r",command);
    if (command == 'q') {
        
        LATAbits.LATA4 = 1;
        //printf("Recieve : %d\n\r Transmit : %d\n\r Error : %d\n\r", flagRecieve, flagTransmit, flagError);
        SPI2BUF = 0x00;
        ES_Event_t SPI_Event;
        SPI_Event.EventType = ES_COMMAND;
        SPI_Event.EventParam = 'O';
        PostTestHarnessService0(SPI_Event);   
    } else if (command == 'w') {
        LATAbits.LATA4 = 0;
        SPI2BUF = 0x00;
        ES_Event_t SPI_Event;
        SPI_Event.EventType = ES_COMMAND;
        SPI_Event.EventParam = 'o';
        PostTestHarnessService0(SPI_Event);   
    } else if (command == 'p') {
        if (PORTBbits.RB4 == 1) {
            SPI2BUF = 7;
            ES_Event_t SPI_Event;
            SPI_Event.EventType = ES_COMMAND;
            SPI_Event.EventParam = 'q';
            PostTestHarnessService0(SPI_Event);        
        } else {
            SPI2BUF = 5;
            ES_Event_t SPI_Event;
            SPI_Event.EventType = ES_COMMAND;
            SPI_Event.EventParam = 'q';
            PostTestHarnessService0(SPI_Event);  
        }
    }
}


/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

