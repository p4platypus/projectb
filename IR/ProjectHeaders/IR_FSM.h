/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef IR_FSMTemplate_H
#define IR_FSMTemplate_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  InitPState, IR_Waiting, IR_Receiving_Receiving, IR_Receiving_Confirming,
          IR_Passing_Transmitting, IR_Passing_Confirming
}IR_State_t;

// Public Function Prototypes

bool Init_IR_FSM(uint8_t Priority);
bool Post_IR_FSM(ES_Event_t ThisEvent);
ES_Event_t Run_IR_FSM(ES_Event_t ThisEvent);
IR_State_t Query_IR_FSM(void);

#endif /* IR_FSMTemplate_H */

