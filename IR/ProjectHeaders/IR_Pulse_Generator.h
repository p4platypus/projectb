/* 
 * File:   IR_Pulse_Generator.h
 * Author: Vivienne
 *
 * Created on February 21, 2021, 11:52 AM
 */

#ifndef IR_PULSE_GENERATOR_H
#define	IR_PULSE_GENERATOR_H

#ifdef	__cplusplus
extern "C" {
#endif




#ifdef	__cplusplus
}
#endif

#endif	/* IR_PULSE_GENERATOR_H */


/*---------------------------- Module Functions ---------------------------*/

void LED_pin_setup(void);
void LED_on(void);
void LED_off(void);
void IR_OC_HW_init(void);

