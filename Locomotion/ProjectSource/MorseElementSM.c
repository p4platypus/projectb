/****************************************************************************
 Module
   MorseElementSM.c

 Description
   This is the main state machine responsible for calibrating and receiving
   standard Morse code in the form of dots and dashes.
****************************************************************************/

/*----------------------------- Include Files -----------------------------*/
// This module
#include "../ProjectHeaders/MorseElementSM.h"

// Hardware
#include <xc.h>
#include <proc/p32mx170f256b.h>
#include <sys/attribs.h> // for ISR macors

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"

/*----------------------------- Module Defines ----------------------------*/
#define BUTTON_INPUT PORTBbits.RB10     // Button input line -- RB10
#define MORSE_INPUT PORTBbits.RB11      // Morse input line -- RB11

/*---------------------------- Module Functions ---------------------------*/
void TestCalibration(void);
void CharacterizeSpace(void);
void CharacterizePulse(void);

/*---------------------------- Module Variables ---------------------------*/
static MorseState_t CurrentState;       // state variable
static uint8_t MyPriority;              // priority variable

static uint16_t TimeOfLastRise;         // time of last Morse rising edge
static uint16_t TimeOfLastFall;         // time of last Morse falling edge
static uint16_t LengthOfDot;            // time length of Morse dot
static uint16_t FirstDelta;             // TimeOfLastFall - TimeOfLastRise
static uint8_t LastInputState;          // Last value from Morse input line
static uint8_t LastButtonState;         // Last value from button input line

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMorseElementSM

 Parameters
     uint8_t: the priority of this service

 Returns
     bool: false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition, and does any
     other required initialization for this state machine
****************************************************************************/
bool InitMorseElementSM(uint8_t Priority)
{
    ES_Event_t ThisEvent;

    // Initialize MyPriority with passed in parameter
    MyPriority = Priority;
    // Configure RB10 as input line to receive button status
    TRISBbits.TRISB10 = 1;
    // Configure RB11 as input line to receive Morse code
    TRISBbits.TRISB11 = 1;
    // Sample Morse input line and use it to initialize LastInputState
    LastInputState = MORSE_INPUT;
    // Set CurrentState to be InitMorseElements
    CurrentState = InitMorseElements;
    // Set FirstDelta to 0
    FirstDelta = 0;
    // Sample button input line and use it to initialize LastButtonState
    LastButtonState = BUTTON_INPUT;
    
    // post the initial transition event
    ThisEvent.EventType = ES_INIT;
    if (ES_PostToService(MyPriority, ThisEvent) == true)
        return true;
    else
        return false;
}

/****************************************************************************
 Function
     PostMorseElementSM

 Parameters
     ES_Event_t: the event to post to the queue

 Returns
     bool: false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
****************************************************************************/
bool PostMorseElementSM(ES_Event_t ThisEvent)
{
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
     RunMorseElementSM

 Parameters
     ES_Event_t: the event to process

 Returns
     ES_Event_t: ES_NO_EVENT if no error ES_ERROR otherwise

 Description
     Calibrates and receives standard Morse code in the form of dots and
     dashes, and then posts events to MorseDecode service for decoding
****************************************************************************/
ES_Event_t RunMorseElementSM(ES_Event_t ThisEvent)
{
    ES_Event_t ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT;

    switch(CurrentState) {
        // If current state is InitMorseElements
        case InitMorseElements:
            // Only if event is ES_INIT
            if(ThisEvent.EventType == ES_INIT) {
                // Go to state CalWaitForRise
                CurrentState = CalWaitForRise;
                // Print status message
                puts("ES_INIT received in MorseElementSM\r");
            }
        break;

        // If current state is CalWaitForRise
        case CalWaitForRise:
            // If event is MORSE_RISE
            if(ThisEvent.EventType == MORSE_RISE) {
                // Set TimeOfLastRise to time from event parameter
                TimeOfLastRise = ThisEvent.EventParam;
                // Go to state CalWaitForFall
                CurrentState = CalWaitForFall;
            }
            // If event is CALIBRATION_COMPLETE
            if(ThisEvent.EventType == CALIBRATION_COMPLETE) {
                // Print LengthOfDot
                printf("\rLengthOfDot = %d\r\n", LengthOfDot);
                // Go to state EOC_WaitRise
                CurrentState = EOC_WaitRise;
            }
        break;
        
        // If current state is CalWaitForFall
        case CalWaitForFall:
            // If event is MORSE_FALL
            if(ThisEvent.EventType == MORSE_FALL) {
                // Set TimeOfLastFall to time from event parameter
                TimeOfLastFall = ThisEvent.EventParam;
                // Go to state CalWaitForRise
                CurrentState = CalWaitForRise;
                // Call TestCalibration to get LengthOfDot
                TestCalibration();
            }
        break;
        
        // If current state is EOC_WaitRise
        case EOC_WaitRise:
            // If event is MORSE_RISE
            if(ThisEvent.EventType == MORSE_RISE) {
                // Set TimeOfLastRise to time from event parameter
                TimeOfLastRise = ThisEvent.EventParam;
                // Go to state EOC_WaitFall
                CurrentState = EOC_WaitFall;
                // Call CharacterizeSpace to post appropriate event
                CharacterizeSpace();
            }
            // If event is BUTTON_DOWN
            if(ThisEvent.EventType == BUTTON_DOWN) {
                // Go to state CalWaitForRise
                CurrentState = CalWaitForRise;
                // Set FirstDelta to 0
                FirstDelta = 0;
            }
        break;
        
        // If current state is EOC_WaitFall
        case EOC_WaitFall:
            // If event is MORSE_FALL
            if(ThisEvent.EventType == MORSE_FALL) {
                // Set TimeOfLastFall to time from event parameter
                TimeOfLastFall = ThisEvent.EventParam;
                // Go to state EOC_WaitRise
                CurrentState = EOC_WaitRise;
            }
            // If event is BUTTON_DOWN
            if(ThisEvent.EventType == BUTTON_DOWN) {
                // Go to state CalWaitForRise
                CurrentState = CalWaitForRise;
                // Set FirstDelta to 0
                FirstDelta = 0;
            }
            // If event is EOC_DETECTED
            if(ThisEvent.EventType == EOC_DETECTED) {
                // Go to state Decode_WaitFall
                CurrentState = Decode_WaitFall;
            }
        break;
        
        // If current state is EOC_WaitRise
        case Decode_WaitRise:
            // If event is MORSE_RISE
            if(ThisEvent.EventType == MORSE_RISE) {
                // Set TimeOfLastRise to time from event parameter
                TimeOfLastRise = ThisEvent.EventParam;
                // Go to state Decode_WaitFall
                CurrentState = Decode_WaitFall;
                // Call CharacterizeSpace to post appropriate event
                CharacterizeSpace();
            }
            // If event is BUTTON_DOWN
            if(ThisEvent.EventType == BUTTON_DOWN) {
                // Go to state CalWaitForRise
                CurrentState = CalWaitForRise;
                // Set FirstDelta to 0
                FirstDelta = 0;
            }
        break;
        
        // If current state is Decode_WaitFall
        case Decode_WaitFall:
            // If event is MORSE_FALL
            if(ThisEvent.EventType == MORSE_FALL) {
                // Set TimeOfLastFall to time from event parameter
                TimeOfLastFall = ThisEvent.EventParam;
                // Go to state EOC_WaitRise
                CurrentState = Decode_WaitRise;
                // Call CharacterizePulse to post appropriate event
                CharacterizePulse();
            }
            // If event is BUTTON_DOWN
            if(ThisEvent.EventType == BUTTON_DOWN) {
                // Go to state CalWaitForRise
                CurrentState = CalWaitForRise;
                // Set FirstDelta to 0
                FirstDelta = 0;
            }
        break;   

        // Repeat cases as required for relevant events
        default:
            ;
    }
    
    return ReturnEvent;
}

/****************************************************************************
 Function
     QueryMorseElementSM

 Parameters
     None

 Returns
     MorseState_t: The current state of the Template state machine

 Description
     Returns the current state of this state machine
****************************************************************************/
MorseState_t QueryMorseElementSM(void)
{
  return CurrentState;
}

/****************************************************************************
 Function
     CheckMorseEvents
 
 Parameters
     None
 
 Returns
     bool: true if a rising / falling edge for a Morse event was detected &
     posted
 
 Description
     Checks to see if a rising / falling edge for a Morse event is detected,
     and if so, gets the current time and posts an MORSE_RISE / MORSE_FALL
     event to this state machine
****************************************************************************/
bool CheckMorseEvents(void)
{   
    // Initialize ReturnVal to false
    bool ReturnVal = false;
    // Get CurrentInputState from Morse input line
    uint8_t CurrentInputState;
    CurrentInputState = MORSE_INPUT;
    
    // If state of Morse input line has changed
    if(CurrentInputState != LastInputState) {
        // If current state of Morse input line is high
        if(CurrentInputState != 0) {
            // Post event MORSE_RISE with parameter of Current Time
            ES_Event_t ThisEvent;
            ThisEvent.EventType = MORSE_RISE;
            ThisEvent.EventParam = ES_Timer_GetTime();
            PostMorseElementSM(ThisEvent);
        // If current state of the input line is low
        } else {
            // Post event MORSE_FALL with parameter of Current Time
            ES_Event_t ThisEvent;
            ThisEvent.EventType = MORSE_FALL;
            ThisEvent.EventParam = ES_Timer_GetTime();
            PostMorseElementSM(ThisEvent);
        }
        // Set ReturnVal to true
        ReturnVal = true;
    }
    
    // Update LastInputState for next call
    LastInputState = CurrentInputState;
    
    return ReturnVal;
}

/****************************************************************************
 Function
     CheckButtonEvents
 
 Parameters
     None
 
 Returns
     bool: true if the re-calibration button is pressed
 
 Description
     Checks to see if the re-calibration button is pressed, and if so, gets
     the current time and posts an BUTTON_DOWN event to this state machine
****************************************************************************/
bool CheckButtonEvents(void)
{
    // Initialize ReturnVal to false
    bool ReturnVal = false;
    // Get CurrentButtonState from button input line
    uint8_t CurrentButtonState;
    CurrentButtonState = BUTTON_INPUT;
    
    // If state of button input line has changed
    if(CurrentButtonState != LastButtonState) {
        // If current state of button input line is down
        if(CurrentButtonState != 0) {
            // Post event BUTTON_DOWN with parameter of Current Time
            ES_Event_t ThisEvent;
            ThisEvent.EventType = BUTTON_DOWN;
            ThisEvent.EventParam = ES_Timer_GetTime();
            PostMorseElementSM(ThisEvent);
            ES_PostToService(1, ThisEvent);
            ES_PostToService(0, ThisEvent);
            // Set ReturnVal to true
            ReturnVal = true;
        } 
    }
    
    // Update LastInputState for next call
    LastButtonState = CurrentButtonState;
    
    return ReturnVal;
}

/***************************************************************************
 Private Functions
 ***************************************************************************/
/****************************************************************************
 Function
     TestCalibration
 
 Parameters
     None
 
 Returns
     None
 
 Description
     Calibrates the length in time of a Morse dot, and once finished, posts
     an CALIBRATION_COMPLETE event to this state machine
****************************************************************************/
void TestCalibration(void) {
    uint16_t SecondDelta;
    
    // If calibration is just starting
    if(FirstDelta == 0) {
        // Set FirstDelta to most recent pulse width
        FirstDelta = TimeOfLastFall - TimeOfLastRise;
    } else {
        // Set SecondDelta to most recent pulse width
        SecondDelta = TimeOfLastFall - TimeOfLastRise;
        // If FirstDelta is 3 times shorter than SecondDelta 
        if(100.0*FirstDelta/SecondDelta <= 33.33) {
            // Set LengthOfDot to FirstDelta
            LengthOfDot = FirstDelta;
            // Post event CALIBRATION_COMPLETE with parameter of Current Time
            ES_Event_t ThisEvent;
            ThisEvent.EventType = CALIBRATION_COMPLETE;
            ThisEvent.EventParam = ES_Timer_GetTime();
            PostMorseElementSM(ThisEvent);
        // If SecondDelta is 3 times shorter than FirstDelta
        } else if(100.0*FirstDelta/SecondDelta > 300.0) {
            // Set LengthOfDot to SecondDelta
            LengthOfDot = SecondDelta;
            // Post event CALIBRATION_COMPLETE with parameter of Current Time
            ES_Event_t ThisEvent;
            ThisEvent.EventType = CALIBRATION_COMPLETE;
            ThisEvent.EventParam = ES_Timer_GetTime();
            PostMorseElementSM(ThisEvent);
        // Prepare for next pulse
        } else {
            // Set FirstDelta to SecondDelta for next iteration
            FirstDelta = SecondDelta;
        }
    }
}

/****************************************************************************
 Function
     CharacterizeSpace
 
 Parameters
     None
 
 Returns
     None
 
 Description
     Characterizes the length in time of a Morse character space, Morse word
     space, or bad space. Once finished, posts an EOC_DETECTED, EOW_DETECTED,
     or BAD_SPACE_DETECTED event, respectively, to MorseElementSM and/or
     MorseDecodeService
****************************************************************************/
void CharacterizeSpace(void) {
    // Calculate last time interval from MORSE_FALL to MORSE_RISE
    uint16_t LastInterval = TimeOfLastRise - TimeOfLastFall;
    
    // If LastInterval is not OK for a dot space 
    if(LastInterval < LengthOfDot || LastInterval > LengthOfDot+1UL) {
        // If LastInterval is OK for a character space
        if(LastInterval >= 3UL*LengthOfDot && LastInterval <= 3UL*(LengthOfDot+1UL)) {
            // Post event EOC_DETECTED to MorseDecodeService & MorseElementSM
            ES_Event_t ThisEvent;
            ThisEvent.EventType = EOC_DETECTED;
            ThisEvent.EventParam = ES_Timer_GetTime();
            PostMorseElementSM(ThisEvent);
            ES_PostToService(1, ThisEvent);
        // If LastInterval is OK for word space    
        } else if(LastInterval >= 7UL*LengthOfDot && LastInterval <= 7UL*(LengthOfDot+1UL)) {
            // Post event EOW_DETECTED to MorseDecodeService
            ES_Event_t ThisEvent;
            ThisEvent.EventType = EOW_DETECTED;
            ThisEvent.EventParam = ES_Timer_GetTime();
            ES_PostToService(1, ThisEvent);
        // Otherwise    
        } else {
            // Post event BAD_SPACE_DETECTED to MorseDecodeService
            ES_Event_t ThisEvent;
            ThisEvent.EventType = BAD_SPACE_DETECTED;
            ThisEvent.EventParam = ES_Timer_GetTime();
            ES_PostToService(1, ThisEvent);
            // Go to state EOC_WaitFall
            CurrentState = EOC_WaitFall;
        }
    }
}

/****************************************************************************
 Function
     CharacterizePulse
 
 Parameters
     None
 
 Returns
     None
 
 Description
     Characterizes the length in time of a Morse dot, Morse dash, or bad pulse.
     Once finished, posts an DOT_DETECTED, DASH_DETECTED, or BAD_PULSE_DETECTED
     event, respectively, to MorseDecodeService
****************************************************************************/
void CharacterizePulse(void) {
    // Calculate last time interval from MORSE_RISE and MORSE_FALL
    uint16_t LastPulseWidth = TimeOfLastFall - TimeOfLastRise;
    
    // If LastPulseWidth is OK for a dot 
    if(LastPulseWidth == LengthOfDot || LastPulseWidth == LengthOfDot+1UL) {
        // Post event DOT_DETECTED to MorseDecodeService
        ES_Event_t ThisEvent;
        ThisEvent.EventType = DOT_DETECTED;
        ThisEvent.EventParam = ES_Timer_GetTime();
        ES_PostToService(1, ThisEvent);
    // If LastPulseWidth is OK for a dash
    } else if(LastPulseWidth >= 3UL*LengthOfDot && LastPulseWidth <= 3UL*(LengthOfDot+1UL)) {
        // Post event DASH_DETECTED to MorseDecodeService
        ES_Event_t ThisEvent;
        ThisEvent.EventType = DASH_DETECTED;
        ThisEvent.EventParam = ES_Timer_GetTime();
        ES_PostToService(1, ThisEvent);
    // Otherwise    
    } else {
        // Post event BAD_PULSE_DETECTED to MorseDecodeService
        ES_Event_t ThisEvent;
        ThisEvent.EventType = BAD_PULSE_DETECTED;
        ThisEvent.EventParam = ES_Timer_GetTime();
        ES_PostToService(1, ThisEvent);
        // Go to state EOC_WaitRise
        CurrentState = EOC_WaitRise;
    } 
}