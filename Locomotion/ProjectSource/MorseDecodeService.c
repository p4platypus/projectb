/****************************************************************************
 Module
   MorseDecodeService.c

 Description
   This is the supplemental service responsible for decoding standard Morse
   code into standard English.
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
// This module
#include "../ProjectHeaders/MorseDecodeService.h"

// Event & Services Framework
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ES_Port.h"

/*----------------------------- Module Defines ----------------------------*/
#define MAX_MORSE_CHAR_SIZE 7                     // max size of MorseString

/*---------------------------- Module Functions ---------------------------*/
MorseChar_t* AddMorseChar(char RegChar);
void CreateMorseCharDictionary(void);
char DecodeMorseString(MorseChar_t* MorseChar, uint8_t i);

/*---------------------------- Module Variables ---------------------------*/
static uint8_t MyPriority;                        // priority variable
static char MorseString[MAX_MORSE_CHAR_SIZE];     // Morse character string
static uint8_t Count;                             // position in MorseString[]
static MorseChar_t* MorseCharDictionary = NULL;   // Morse character dictionary

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitMorseDecodeService

 Parameters
     uint8_t: the priority of this service

 Returns
     bool: false if error in initialization, true otherwise

 Description
     Saves away the priority, sets up the initial transition, and does any
     other required initialization for this service
****************************************************************************/
bool InitMorseDecodeService(uint8_t Priority)
{
    ES_Event_t ThisEvent;
    
    // Initialize MyPriority with passed in parameter
    MyPriority = Priority;
    // Clear MorseString 
    Count = 0;
    // Create Morse character dictionary
    CreateMorseCharDictionary();

    // post the initial transition event
    ThisEvent.EventType = ES_INIT;
    if (ES_PostToService(MyPriority, ThisEvent) == true)
        return true;
    else
        return false;
}

/****************************************************************************
 Function
     PostMorseDecodeService

 Parameters
     ES_Event_t: the event to post to the queue

 Returns
     bool: false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
****************************************************************************/
bool PostMorseDecodeService(ES_Event_t ThisEvent)
{
    return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
     RunMorseDecodeService

 Parameters
     ES_Event_t: the event to process

 Returns
     ES_Event_t: ES_NO_EVENT if no error ES_ERROR otherwise

 Description
     Calibrates and receives standard Morse code in the form of dots and
     dashes, and then posts events to MorseDecode service for decoding
****************************************************************************/
ES_Event_t RunMorseDecodeService(ES_Event_t ThisEvent)
{
    ES_Event_t ReturnEvent;
    ReturnEvent.EventType = ES_NO_EVENT;
  
    switch(ThisEvent.EventType) {
        // If event is DOT_DETECTED
        case DOT_DETECTED:
            // If there is room for another Morse element in MorseString[]
            if(Count < MAX_MORSE_CHAR_SIZE) {
                // Add a Dot to MorseString[]
                MorseString[Count] = '.';
                Count++;
            // Otherwise
            } else {
                // Set ReturnEvent to ES_ERROR
                ReturnEvent.EventType = ES_ERROR;
                ReturnEvent.EventParam = '.';
            }
        break;
        
        // If event is DASH_DETECTED
        case DASH_DETECTED:
            // If there is room for another Morse element in MorseString[]
            if(Count < MAX_MORSE_CHAR_SIZE) {
                // Add a Dash to MorseString[]
                MorseString[Count] = '-';
                Count++;
            // Otherwise
            } else {
                // Set ReturnEvent to ES_ERROR
                ReturnEvent.EventType = ES_ERROR;
                ReturnEvent.EventParam = '-';
            }
        break;
        
        // If event is EOC_DETECTED
        case EOC_DETECTED:
            ;
            // Call DecodeMorseString to try and match current MorseString[]
            char RegCharEOC = DecodeMorseString(MorseCharDictionary, 0);
            // Print decoded character to OLED
            printf("%c", RegCharEOC);
            ES_Event_t CharEvent;
            CharEvent.EventType = ES_NEW_KEY;
            CharEvent.EventParam = RegCharEOC;
            ES_PostToService(0, CharEvent);
            // Clear MorseString
            Count = 0;
        break;
        
        // If event is EOW_DETECTED
        case EOW_DETECTED:
            ;
            // Call DecodeMorseString to try and match current MorseString[]
            char RegCharEOW = DecodeMorseString(MorseCharDictionary, 0);
            // Print decoded character to OLED
            printf("%c", RegCharEOW);
            ES_Event_t WordEvent1;
            WordEvent1.EventType = ES_NEW_KEY;
            WordEvent1.EventParam = RegCharEOW;
            ES_PostToService(0, WordEvent1);
            // Print space to OLED
            printf(" ");
            ES_Event_t WordEvent2;
            WordEvent2.EventType = ES_NEW_KEY;
            WordEvent2.EventParam = ' ';
            ES_PostToService(0, WordEvent2);
            // Clear MorseString
            Count = 0;
        break;
        
        // If event is BAD_SPACE_DETECTED
        case BAD_SPACE_DETECTED:
            // Clear MorseString
            Count = 0;
        break;
        
        // If event is BAD_PULSE_DETECTED
        case BAD_PULSE_DETECTED:   
            // Clear MorseString
            Count = 0;
        break;
        
        // If event is BUTTON_DOWN
        case BUTTON_DOWN:  
            // Clear MorseString
            puts("\rCALIBRATING...");
            Count = 0;
        break;
        
        // Repeat cases as required for relevant events
        default:
            ;
    }
  
    return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
/****************************************************************************
 Function
     AddMorseChar
 
 Parameters
     char: the regular character corresponding to the Morse character
 
 Returns
     MorseChar_t*: a pointer to the created Morse character
 
 Description
     Adds a Morse character struct that contains its regular character to the
     Morse character dictionary
****************************************************************************/
MorseChar_t* AddMorseChar(char RegChar) {
    // Allocate memory for new MorseChar  
    MorseChar_t* MorseChar = (MorseChar_t*)malloc(sizeof(MorseChar_t)); 
  
    // Assign new MorseChar its corresponding RegChar
    MorseChar->RegChar = RegChar; 
  
    // Initialize new MorseChar's left and right children as NULL 
    MorseChar->Dot = NULL; 
    MorseChar->Dash = NULL; 
    
    return(MorseChar); 
}

/****************************************************************************
 Function
     CreateMorseCharDictionary
 
 Parameters
     None
 
 Returns
     None
 
 Description
     Constructs a Morse character dictionary in the form of a binary tree.
     It maps each Morse character to its corresponding regular character.
     It also maps some invalid Morse characters to '~', as needed for the
     construction of binary tree.
****************************************************************************/
void CreateMorseCharDictionary(void) {
    // Only construct Morse character dictionary if it is non-existent
    if(MorseCharDictionary == NULL) {
        // Add all letter entries
        MorseCharDictionary = AddMorseChar(' ');
        MorseCharDictionary->Dot = AddMorseChar('E');
        MorseCharDictionary->Dash = AddMorseChar('T');
        MorseCharDictionary->Dot->Dot = AddMorseChar('I');
        MorseCharDictionary->Dot->Dash = AddMorseChar('A');
        MorseCharDictionary->Dash->Dot = AddMorseChar('N');
        MorseCharDictionary->Dash->Dash = AddMorseChar('M');
        MorseCharDictionary->Dot->Dot->Dot = AddMorseChar('S');
        MorseCharDictionary->Dot->Dot->Dash = AddMorseChar('U');
        MorseCharDictionary->Dot->Dash->Dot = AddMorseChar('R');
        MorseCharDictionary->Dot->Dash->Dash = AddMorseChar('W');
        MorseCharDictionary->Dash->Dot->Dot = AddMorseChar('D');
        MorseCharDictionary->Dash->Dot->Dash = AddMorseChar('K');
        MorseCharDictionary->Dash->Dash->Dot = AddMorseChar('G');
        MorseCharDictionary->Dash->Dash->Dash = AddMorseChar('O');
        MorseCharDictionary->Dot->Dot->Dot->Dot = AddMorseChar('H');
        MorseCharDictionary->Dot->Dot->Dot->Dash = AddMorseChar('V');
        MorseCharDictionary->Dot->Dot->Dash->Dot = AddMorseChar('F');
        MorseCharDictionary->Dot->Dot->Dash->Dash = AddMorseChar('~');
        MorseCharDictionary->Dot->Dash->Dot->Dot = AddMorseChar('L');
        MorseCharDictionary->Dot->Dash->Dot->Dash = AddMorseChar('~');
        MorseCharDictionary->Dot->Dash->Dash->Dot = AddMorseChar('P');
        MorseCharDictionary->Dot->Dash->Dash->Dash = AddMorseChar('J');
        MorseCharDictionary->Dash->Dot->Dot->Dot = AddMorseChar('B');
        MorseCharDictionary->Dash->Dot->Dot->Dash = AddMorseChar('X');
        MorseCharDictionary->Dash->Dot->Dash->Dot = AddMorseChar('C');
        MorseCharDictionary->Dash->Dot->Dash->Dash = AddMorseChar('Y');
        MorseCharDictionary->Dash->Dash->Dot->Dot = AddMorseChar('Z');
        MorseCharDictionary->Dash->Dash->Dot->Dash = AddMorseChar('Q');
        MorseCharDictionary->Dash->Dash->Dash->Dot = AddMorseChar('~');
        MorseCharDictionary->Dash->Dash->Dash->Dash = AddMorseChar('~');
        
        // Add all numeric entries
        MorseCharDictionary->Dot->Dash->Dash->Dash->Dash = AddMorseChar('1');
        MorseCharDictionary->Dot->Dot->Dash->Dash->Dash = AddMorseChar('2');
        MorseCharDictionary->Dot->Dot->Dot->Dash->Dash = AddMorseChar('3');
        MorseCharDictionary->Dot->Dot->Dot->Dot->Dash = AddMorseChar('4');
        MorseCharDictionary->Dot->Dot->Dot->Dot->Dot = AddMorseChar('5');
        MorseCharDictionary->Dash->Dot->Dot->Dot->Dot = AddMorseChar('6');
        MorseCharDictionary->Dash->Dash->Dot->Dot->Dot = AddMorseChar('7');
        MorseCharDictionary->Dash->Dash->Dash->Dot->Dot = AddMorseChar('8');
        MorseCharDictionary->Dash->Dash->Dash->Dash->Dot = AddMorseChar('9');
        MorseCharDictionary->Dash->Dash->Dash->Dash->Dash = AddMorseChar('0');
        
        // Add all punctuation entries
        MorseCharDictionary->Dot->Dot->Dash->Dash->Dot = AddMorseChar('~');
        MorseCharDictionary->Dot->Dot->Dash->Dash->Dot->Dot = AddMorseChar('?');
        MorseCharDictionary->Dot->Dash->Dot->Dash->Dot = AddMorseChar('+');
        MorseCharDictionary->Dot->Dash->Dot->Dash->Dot->Dash = AddMorseChar('.');
        MorseCharDictionary->Dash->Dash->Dot->Dot->Dash = AddMorseChar('~');
        MorseCharDictionary->Dash->Dash->Dot->Dot->Dash->Dash = AddMorseChar(',');
        MorseCharDictionary->Dash->Dash->Dash->Dot->Dot->Dot = AddMorseChar(':');
        MorseCharDictionary->Dot->Dash->Dash->Dash->Dash->Dot = AddMorseChar('\'');
        MorseCharDictionary->Dash->Dot->Dot->Dot->Dot->Dash = AddMorseChar('-');
        MorseCharDictionary->Dash->Dot->Dot->Dash->Dot = AddMorseChar('/');
        MorseCharDictionary->Dash->Dot->Dash->Dash->Dot = AddMorseChar('(');
        MorseCharDictionary->Dash->Dot->Dot->Dash->Dot = AddMorseChar('/');
        MorseCharDictionary->Dash->Dot->Dash->Dash->Dot->Dash = AddMorseChar(')');
        MorseCharDictionary->Dot->Dash->Dot->Dot->Dash = AddMorseChar('~');
        MorseCharDictionary->Dot->Dash->Dot->Dot->Dash->Dot = AddMorseChar('"');
        MorseCharDictionary->Dash->Dot->Dot->Dot->Dash = AddMorseChar('=');
        MorseCharDictionary->Dash->Dot->Dash->Dot->Dash = AddMorseChar('~');
        MorseCharDictionary->Dash->Dot->Dash->Dot->Dash->Dash = AddMorseChar('!');
        MorseCharDictionary->Dot->Dot->Dot->Dash->Dot = AddMorseChar('~');
        MorseCharDictionary->Dot->Dot->Dot->Dash->Dot->Dot = AddMorseChar('~');
        MorseCharDictionary->Dot->Dot->Dot->Dash->Dot->Dot->Dash = AddMorseChar('$');
        MorseCharDictionary->Dot->Dash->Dot->Dot->Dot = AddMorseChar('&');
        MorseCharDictionary->Dash->Dot->Dash->Dot->Dash->Dot = AddMorseChar(';');
        MorseCharDictionary->Dot->Dash->Dash->Dot->Dash = AddMorseChar('~');
        MorseCharDictionary->Dot->Dash->Dash->Dot->Dash->Dot = AddMorseChar('@');
        MorseCharDictionary->Dot->Dot->Dash->Dash->Dot->Dash = AddMorseChar('_');
    }
}

/****************************************************************************
 Function
     DecodeMorseString
 
 Parameters
     MorseChar_t*: the current node in MorseCharDictionary
     uint8_t: the current position in MorseString[]
 
 Returns
     char: the regular character corresponding to the Morse character
 
 Description
     Searches through the Morse character dictionary and returns the regular
     character corresponding to the Morse character in MorseString[]. Returns
     '~' if a match cannot be found.
****************************************************************************/
char DecodeMorseString(MorseChar_t* MorseChar, uint8_t i) {
    // Initialize RegChar
    char RegChar;
    
    // Return '~' if match cannot be found MorseChar == NULL
    if(0) {
        RegChar = '~';
    // Otherwise
    } else {
        // Return corresponding regular character if match is found
        if(i == Count)
            RegChar = MorseChar->RegChar;
        // Recursively traverse down Dot branch of MorseCharDictionary
        // and go to next Morse element if current Morse element is Dot
        else if(MorseString[i] == '.')
            RegChar = DecodeMorseString(MorseChar->Dot, i+1);
        // Recursively traverse down Dash branch of MorseCharDictionary
        // and go to next Morse element if current Morse element is Dash
        else if(MorseString[i] == '-')
            RegChar = DecodeMorseString(MorseChar->Dash, i+1);           
        // Return '~' if invalid Morse element is in MorseString[]
        else
            RegChar = '~';
    }
    
    return RegChar;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/


