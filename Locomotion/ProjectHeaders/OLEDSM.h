/****************************************************************************
  Header file for OLEDSM
  based on the Gen2 Events and Services Framework
 ****************************************************************************/

#ifndef OLEDSM_H
#define OLEDSM_H

// Event Definitions
#include "ES_Events.h"    /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Port.h"      /* defines serial routines */

// State definitions for use with the query function
typedef enum {
    InitPState,
    Waiting,
    Writing
} OLEDState_t;

// Public Function Prototypes
bool InitOLEDSM(uint8_t Priority);
bool PostOLEDSM(ES_Event_t ThisEvent);
ES_Event_t RunOLEDSM(ES_Event_t ThisEvent);
OLEDState_t QueryOLEDSM(void);
bool Check4NextPage(void);

#endif /* OLEDSM_H */

