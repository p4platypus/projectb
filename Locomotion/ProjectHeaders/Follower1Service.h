/****************************************************************************
  Header file for Follower1Service
  based on the Gen2 Events and Services Framework
 ****************************************************************************/

#ifndef Follower1Service_H
#define Follower1Service_H

// Event Definitions
#include "ES_Events.h"    /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */
#include "ES_Port.h"      /* defines serial routines */

// Public Function Prototypes
bool InitFollower1Service(uint8_t Priority);
bool PostFollower1Service(ES_Event_t ThisEvent);
ES_Event_t RunFollower1Service(ES_Event_t ThisEvent);

#endif /* Follower1Service_H */